FROM node:14.17.0-alpine as build-step
WORKDIR /app
COPY package.json /app
RUN npm install --silent
COPY . /app
RUN npm run build --prod

FROM nginx
COPY ./nginx.conf /etc/nginx/nginx.conf 
COPY --from=build /app/build /var/www/react-app
CMD ["nginx", "-g", "daemon off;"]
